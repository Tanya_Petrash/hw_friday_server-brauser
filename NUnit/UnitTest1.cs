using NUnit.Framework;
using NetworksImitation;
using System.Collections.Generic;

namespace NUnit_Tests
{
    public class Tests
    {



        [TestCase("��� �������� ������������ �������� ���������", "��� �������� ������������ �������� ���������")]
        public void Test1(string text, string expectedresult)
        {
            Browser browser = new Browser();
            List<string> Requests = new List<string>();
            Requests = browser.CreateRequests();
            Assert.IsNotEmpty(Requests, "Mistake!!!");
        }

        [TestCase("C8fNF6yHsyD32y4i/nclD8Dk1VkCDcOb1tn/5H9SxZf4FsD5yFOntk5+wK/cxp8gyi5cFnIM6B1bg03czg+F4JrGoas+9VsSWCdzyo1CGG4p4lyZluRLJDt8BkIvf+HwTH9gXUoQztYqE/5OfIUK84f2lXCrA8LDBRK26625Ntg=", "ZvUIhk/ODJqEvNaeXSeT34kx42af/pAjwA0B1JqSCpKV7IjvWr8y88sLWvuPAsvsjeCXNAo0gxDANOsTOZWRLSVqWqGPwG/fczLsG7c78N33+EKFSorP1/sjjQVOe+VX")] // ����������� 6� ����� ������� �����������
        public void Test2(string binaryStr, string expectedresult)
        {
            Browser browser = new Browser();

            string acrualresult = browser.GetData(binaryStr);
            Assert.AreEqual(expectedresult, acrualresult, "Mistake");
        }

        [TestCase("�������� ������ ���������", "�������� ������ ���������")]
        public void Test3(string text, string expectedresult)
        {
            DataBase dataBase = new DataBase();
            List<string> UserDataBase = new List<string>();
            UserDataBase = dataBase.AddInfoIntoDataBase();
            Assert.IsNotEmpty(UserDataBase, "Mistake!!!");
        }
    }
}