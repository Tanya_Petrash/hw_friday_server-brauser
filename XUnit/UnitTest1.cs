using System;
using Xunit;
using NetworksImitation;
using System.Collections.Generic;


namespace xUnit_Tests
{
    public class UnitTest1
    {

        [Fact]
        public void Test1()
        {
            Server server = new Server();

            Dictionary<string, string> Responses = new Dictionary<string, string>();
            Responses = server.CreateResponses();

            Assert.NotEmpty(Responses);
        }

        [Fact]
        public void Test2()
        {
            Server server = new Server();
            string actresult = server.GetInfoFromDataBase();
            Assert.NotEmpty("����� ���� ����������");
        }

        [Fact]
        public void Test3()
        {
            Server server = new Server();
            Dictionary<string, string> Responses = new Dictionary<string, string>();
            string actresult = server.ChangeStringToMachineCode("��� �������� ������������ �������� ���������", Responses);
            Assert.Equal("ZvUIhk/ODJqEvNaeXSeT34kx42af/pAjwA0B1JqSCpKV7IjvWr8y88sLWvuPAsvsjeCXNAo0gxDANOsTOZWRLSVqWqGPwG/fczLsG7c78N33+EKFSorP1/sjjQVOe+VX", actresult);
        }

        [Fact]
        public void Test4() // ��������� �� �������� ������,������� �������
        {

            Server server = new Server();
            //KeyValuePair<string, string> kvp = new KeyValuePair<string, string>();
            Dictionary<string, string> Responses = new Dictionary<string, string>();
            string browserresponse = "";
            Responses = server.RemoveData(browserresponse);
            string expectedresult = "��� �������� ������������ �������� ���������";
            Assert.DoesNotContain(expectedresult, browserresponse);
        }

        [Fact]
        public void Test5() // � ���� �����,�������� �������� � ���� ����������
        {
            Server server = new Server();
            Socket socket = new Socket();
            string Coockie = "��� �������� ������������ �������� ���������";
            string acrualresult = socket.DataTransferToServer(server.ChangeStringToMachineCode(Coockie, server.Responses));
            Assert.Equal("C8fNF6yHsyD32y4i/nclD8Dk1VkCDcOb1tn/5H9SxZf4FsD5yFOntk5+wK/cxp8gyi5cFnIM6B1bg03czg+F4JrGoas+9VsSWCdzyo1CGG4p4lyZluRLJDt8BkIvf+HwTH9gXUoQztYqE/5OfIUK84f2lXCrA8LDBRK26625Ntg=", acrualresult);

        }
        [Fact]
        public void Test6()
        {
            Server server = new Server();
            Socket socket = new Socket();
            Browser browser = new Browser();
            string binaryStr = "C8fNF6yHsyD32y4i/nclD8Dk1VkCDcOb1tn/5H9SxZf4FsD5yFOntk5+wK/cxp8gyi5cFnIM6B1bg03czg+F4JrGoas+9VsSWCdzyo1CGG4p4lyZluRLJDt8BkIvf+HwTH9gXUoQztYqE/5OfIUK84f2lXCrA8LDBRK26625Ntg=";

            string acrualresult = socket.DataTransferToBrowser(binaryStr);
            Assert.Equal("ZvUIhk/ODJqEvNaeXSeT34kx42af/pAjwA0B1JqSCpKV7IjvWr8y88sLWvuPAsvsjeCXNAo0gxDANOsTOZWRLSVqWqGPwG/fczLsG7c78N33+EKFSorP1/sjjQVOe+VX", acrualresult);

        }
    }
}